
"How could a name hurt someone?" one of the children, simultaneously raising their hand as they spoke.

"Back then, people didn't choose their own name, their parents chose it for them, often before they were born." The history teacher began.

"What?? That doesn't make sense!"

Another kid chimed in, "how would the parent even know what the baby likes, or what the baby is like? Babies don't talk!"

"Yes, but they did differently back then than we do now."

"There wouldn't be so many people who hurt if the parents just let 'em choose their own names!"

"At the time, it was a long tradition! A 'given name'. People could change their name, but it was much harder than it is now, they would have to fill out all sorts of paperwork and give a reason why they were changing it."

"Why?? What if you just WANT to?"

One child, who was always VERY serious about their homework, decided to probe the topic a little deeper. "How can having the wrong name hurt?" Their stylus was at the ready on top of their tablet.

"Sometimes, a name just doesn't feel right. But also," the teacher paused,

"...Back then, people also got separated into two categories."

"Huh?" Multiple children in the class looked puzzled.

"From birth, most people were given a NAME, but also a CLASSIFICATION, and they weren't allowed to change either - at least, not easily."

"This was 'boy' and 'girl', right?" The note-taking student asked aloud - they had done the reading ahead of time, unlike most of the kids.

"Right. Based on which one was chosen for a person, it would dictate their role in life, how they were treated, all sorts of things."

"For a long time, one role had no rights and was considered property of the other. They spent their days inside the home, cooking and cleaning and raising children. The other role kept all the power for themselves."

"That's not fair!" One child exclaimed.

"Teacher, this doesn't make any sense!"

"Yeah, why would you just choose a bad role for one person and a good role for another?"

"Why didn't they fight back?"

"What about now?"

"What if their parents were wrong?"

The teacher eyed their watch briefly. So many questions, and not much time left. 

"OK, class. Here are the questions you're asking." The teacher was scribbling their questions on the board. "I want you to think about these as your homework. Let's continue discussing tomorrow."
