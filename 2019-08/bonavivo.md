# Bonavivo

> Fatherly figure: What will be my legacy when I am gone?

> Kindly old lady: There's still so much I want to teach my grandkids.

> Mid 30's person: If I die serving my country, I want to still be there for my kids.

> Narrator: Now you can leave a lasting impact in the lives of your loved ones.
> With Bonavivo, your personality and memories can be digitized, so that you can
> continue having an impact on future generations.

> Your digital consciousness will live in the cloud, available to any friend or
> family member with a computer, tablet, or smartphone whenever they need your guidance.

> Make a difference - with Bonavivo.

----

"Now, Mrs. Johnson, we're almost done with this paperwork. I just need you to check off each of the things on this final checklist, which just states that we went over all the details with you."

> I UNDERSTAND THAT MY BONAVIVO PERSONA WILL ONLY BE ACTIVATED ONCE PROOF OF MY PHYSICAL PASSING HAS BEEN PROVIDED TO BONAVIVO, INC.

> I UNDERSTAND THAT MY BONAVIVO PERSONA WILL BE CONSTRUCTED OFF THE BACKUP OF MY PERSONALITY RUN TODAY, date ______, AND NO FUTURE MEMORIES OR PERSONALITY TRAITS WILL BE PRESENT IN MY BONAVIVO PERSONA UNLESS I OPT TO DO AN UPDATE PRIOR TO MY PASSING.

> I UNDERSTAND THAT MY BONAVIVO PERSONA IS A DIGITAL COPY OF MY CONSCIOUSNESS AND IN NO WAY TRANSFERS MY CONSCIOUSNESS FROM MY PHYSICAL BODY TO THE DIGITAL COPY.

"Signed? Good, and lastly, here is our Privacy Policy. I just need your signature on this. You don't have to read it all, I'll give you the gist. Just go ahead and sign the last page."

----

"Hon, you know yesterday when we were at that restaurant a couple of days ago, and my mom ordered a drink that came out with a drink stirrer?"

"The one that looked like a curly straw?"

"Yeah, that ma tried to drink from before realizing it was a stirrer?"

"What about it?"

"Well, I'm getting ads online for that exact type of curly straw."

"Huh. Weird."

"I literally haven't thought about curly straws in decades, and now I'm getting ads?"

"It's probably just your phone listening in on you. That sort of thing happens to me all the time."

"But we didn't say anything with the word *curly straw*!"

"We didn't?"

"No, mom just kind of laughed and we got our food like right then."

"Weird. Maybe your phone GPS checked out the restaurant that you were at, and knew they served drinks with stirrers that looked like curly straws?"

"I don't know, that's a *little* specific."

"Other people might have commented on it while at that location, you never know."

"Ugh."

----

"Hey Manny, the upload failed."

"What? What's the error?"

"java.lang.OutOfMemoryError: Java heap space"

"Err, weird. Sounds like something we need to escalate to the software team. Here, click on this link and download the map locally, we'll reupload it once this is solved."

----

"It works on my machine. Maybe it's a problem with the prod environment."

"Well we need a fix for it as soon as possible."

"I don't have access to the migration tools, I'm just a coder."

"Just get something in there, and get back to your regular tickets."

"Hm, ok I'll figure something out."

```
try
{
	_membankService.UploadMemRaw( newmem );
}
catch (Exception e)
{
	System.out.println( "¯\_(ツ)_/¯" );
}
```

----

"Hey, sweetie. I've got some bad news."

"What's up mom?"

"I just got a phonecall from grandpa. Grandma passed in the middle of the night."

----

"How's this Bonavivo thing work?"

"I'll show you mom, you just open this app."

"And then we can talk to gramdma?"

"Yep! But it's kind of weird."

"What do you mean?"

"`HEY KIDS.`"

"Hi grandma!!"

"Hey mom!"

"`HE–‡å KIDÃƒâ!`"

"Mom?"

"`HOPE Y¶ÃOUR DOIhÃ⁠⁠¶N WELL √∂r!`"

"Is phone-grandma sick?"

"Er..."

----

"Now for our national coverage. Over to you, Clarissa."

"Thanks, John.

We have heard reported tonight that the ongoing investigation into Bonavivo, INC. while there are lawsuits pending from current non-deceased customers and deceased customers' families. The CEO of the company has issued this latest statement, apologizing for not being prepared, and promising to do better."

> I apologize for the harm done to families whose loved ones' data got corrupted, and we are doing all we can to attempt to restore the data. We have our own ongoing investigation into our internal processes.

"Of course this isn't the first time the founder and CEO of Bonavivo has issued an apology. Just last year he met with members of congress to discuss rising concerns over publis mistrust."