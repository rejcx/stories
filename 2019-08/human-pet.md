# Human Pet

## Morning!

It's morning! Time for food! I'm so excited for Mom to wake up and feed us!!

I sit in my fuzzy bed in the entry room, where Mom expects me to be when she wakes up. She won't feed us if we aren't sitting in our beds, quiet and obedient.

The sun woke me up, but I know it will be a bit before Mom wakes up. I don't know how she wakes up, but when she does the sun is always at the same position.

My sibling (not by blood, just another one of us) isn't in his bed. He's sitting next to the door to Mom's room, where she is probably still sleeping.

"Hey." I call to him.

"I'm hungry." He responds.

"Don't bother Mom."

"BUT I'M SO HUNGRY."

"She'll get mad."

"AGGH." He continues sitting next to the door. Periodically, he knocks softly on the door, but Mom won't come out until she's ready.

Gramps (also not related by blood) slowly walks over to his bed and sits down.

"Gramps, Kevin is trying to bother Mom." I tattle-tell.

"Kevin, get over here." Gramps requests, but Kevin stays put.

"I'm HUNGRY." Kevin says, hangrily.

"Mom won't come out until her waking time."

"I want to know EXACTLY WHEN SHE GETS UP."

Gramps huffs and lays down in his own fuzzy bed next to mine.

"It's best to just go along with the routine." He says.

"Gramps, tell me about when we were wild." I request. I like to dream about being a wild human when I go to bed at night, because it sounds so cool.

"Hmph. It was a harder life than we have now."

"How did we get our food before we had a Mom?"

"We would drive cars from our home building to another building..."

"And that's where the food was stored?"

"Well, first, we had to work, in order to get our food."

"What's a car? What's work?" I ask.

Gramps grumbles and thinks for a moment. "Um. A car. So we wouldn't have to walk. It was like Mom's ship, but on the ground."

"Oh I hate that thing. Any time Mom takes us in her ship we go to the vet."

"But we would drive our own."

"WOAH!"

"And usually it was to go to work."

"I would drive to the moon!" Kevin says, finally jumping into his bed to listen.

"Some humans did drive to the moon, long long ago." Gramps remembers, "We would see stories about it on the teevee."

"What's a--" Kevin starts.

Suddely, there's a sliding sound as Mom's door slides sideways and closes behind her. She walks into the entry room and into the kitchen, satisfied that we are all in our beds.

"╉╓┵╗┚╸" Mom greets us. We don't understand her language, but we can make out certain patterns. For example, when she calls, I'm "╿╬", Kevin is "┳╺╝", and Gramps is "╓╏".

"You know, it's funny. When we used to make up stories about extra-terrestrials back in my day, we always imagined 'em as being these skinny gray things, about the same height as us, or maybe shorter." Gramps reminisces.

Mom wasn't short, and she wasn't gray, and she didn't really look like us. Most of the time, she moved around on her thin, stick-like legs, supporting her orbular body. She had two eyes, like us, and her skin was similar to ours, but more of a purple color. She wore soft shells around her body that were different colors, kind of like how she put clothes on us when it was cold outside, but ours were made of Homeplanet fabrics, like made from BaahBaah hair.

We had *some* similarities to Mom. However, manipulated the world around her in a way we didn't understand - we had hands, we could grab our toy wildebeast and bat it around, but Mom always made things happen in a way we could not perceive.

Once Mom was in the kitchen, the bag of food came out. Every day we got chunks of dry chicken-and-veggie kibble. Sometimes, we'd get special food, like chicken and veggies in a sauce. Kevin and I like the food fine, it's whatever, but Gramps always seems frustrated at it. I suppose that he had other foods when he was a Wild Gramps, and maybe he's bored of the routine.

![A sketch of me and mom!](images/Mom.png)

## Afternoon!

Mom always leaves the house during the day. She takes her ship somewhere, leaving us behind. She only takes us with her when we go to the Vet, which means that they poke and prod at us, or stick us with needles, or sometimes put us to sleep and cut us open. Kevin recently was cut and part of him removed. He had to have cones put on his hands for weeks because he kept trying to pick at the spot where they removed his lower parts. I don't know *why* this happens, but it happened to me, too. Except I didn't have those things down there *to remove*, so why even bother?

The days are pretty peaceful, though sometimes boring. We look out the window at the life happening outside. Sometimes, we see Outdoor Humans wandering around. They go for walks, they chase other animals, and they poop in our yard. We wave and sometimes they wave back. Sometimes, they might try to talk to us with their hands. Only Gramps knows Hand Language, and can talk back. I've thought of learning Hand Language but not that many Outdoor Humans use it, so I spend my time relaxing instead.

Gramps always seems the most restless at this time. I know he's really smart, and did all sorts of things when he was a Wild Human, so I know that living with Mom is kind of boring by comparison. But there are no more Wild Humans, not really. At least, not on this planet. Maybe back on the Homeplanet some humans managed to survive, maybe they're back to going to Work in their Cars to buy Food. I don't know!

But all the humans on this planet - I guess we call it Momsplanet? I don't know what the Outdoor Humans call it - we either all belong to a Mom, or maybe we don't have a Mom and live outside all the time, but food can be hard to find out there.

If Mom let us out, I'd be afraid of getting lost, and if I couldn't find Mom, how would I get food? What if I get cold? I know Mom loves us and she always takes care of us.

Kevin is less smart. He doesn't think about these things. Sometimes, he tries to run out the door. But he's not very fast, and Mom catches him. I lecture him about doing that, how he might get lost and starve to dead, but he doesn't think.

Gramps says he wouldn't know how to survive out there and just stays put. He doesn't even look out the window most days, just when we call him to translate Hand Language. That's ok, I don't want to bother him too much.

The sun is up in the sky and it is getting warm inside. I move my bed onto the floor where the light warms the area and I lay down, covering myself with my fuzzy blanket. I love the feel of the fuzzy bed below and the soft blanket above on my bare skin, and the warmth. It's very easy to doze off.

![A drawing of me in my bed!](images/bed.png)

## Evening!

I hear Mom's ship land and I roll over, readjusting my blanket. No need to get up, really. As I was sleeping, Kevin came to curl up next to me and we nap together.

The door slides up and Mom comes in. "╓┤╋┶╄━╭╄┊ ╒╃┚╮┨┲." she greets us.

Her things - floating slightly behind her - fade away, going to storage. Mom makes her way to the kitchen to make sure we have water (we do) and food (we don't).

She refills our bowls and Kevin gets up. The only thing better than sleep is food (for him, at least). I stretch and roll over and continue dozing, listening to what's happening around me.

"┾┺┌┒╊╋ ╓╏?" Mom asks. I hear her say Gramps' name, "╓╏".

I listen to her walk around, and to Kevin eating, as I enjoy my soft blanket. Mom walks here and there, to each room of the house. Then I hear a strange sound coming from Mom.

Kevin and I both follow Mom to see what's wrong. She is in her room, and Gramps is laying next to a heater vent where he was taking a nap. Mom probes at Gramps here and there with her legs and with her invisible manipulators, Gramps not waking up.

"Is Gramps a dead?" Kevin asks. Gramps warned us about this. I haven't seen a dead thing before, but Gramps said that for humans,  it's like you can't wake them up from sleeping. It's more likely to happen if you're old, or if you're hungry for too long, or if you're too cold or too hot, or if you're too rough with another human.

Mom picks up Gramps with her invisible abilities and we follow as she leaves the room.

I've only lived with Mom for a couple of years, Kevin for maybe one year, and gramps for a dozen years, but I know he was much older. He said maybe seven of tens of years maybe. You could tell from his shaggy gray hair and droopy skin. Sometimes humans go to different Moms throughout their life because we do live a lot of tens of years, though I don't know how long Moms live, or what they look like when they are dead.

Mom sets Gramps down in the Mom waste compartment (where Mom poops, we Poop in a ceramic bowl because we're house trained) and Gramps is whisked away by a stream of water to who knows where. I hope Gramps doesn't have to spend his dead sleeping in poop.

Mom is sluggish for the rest of the day. I think maybe she's sad. And I'm sad, too. I loved Gramps, Kevin loved Gramps, and we will miss him and his stories. He was our only connection to knowing about when humans were wild, and now I'll have to do my best to keep telling his stories to other humans, if more come to live with us.

![A drawing of Dead Gramps](images/Gramps.png)

