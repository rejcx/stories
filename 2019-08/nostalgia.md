# Nostalgia

"Sometimes I feel like I was born in the wrong time period."
`f8d61f2d-9cc4-42bc-8087-19e08d0e1da7` (often addressed as "eldat") stretched her arms and straightened her back, trying to shake off the stiffness from sitting all day.

"Oh yeah?" responded her buddy, `56518831-04aa-49e2-ad0e-fa1874bf0tee` (often called "fotee").

"I mean, I wish I could go back to the simple times, like when people just communicated on those smartphone things. You could just, put 'em down, y'know? None of this constant surveillance and communication through the Mindnet."

Fotee was only half paying attention. They were currently trying to diagnose their algorithm for better accuracy and performance, and was honestly a bit miffed that Eldat had resorted to speaking verbally instead of mentally.

"I dunno, it's easy to talk over Mindnet."

"Yeah, but it never goes away. If my client pings me at any hour, they know I'm available... it's my BRAIN, I always get the message."

Eldat was mentally searching for new clients as Fotee grumpily adjusted their data set and tried to re-train their algorithm.

"Uh huh. Have you found a new client yet?"

"No, no new gigs for our algorithm. Not unless we want to undersell like everybody else."

Fotee grumbled.

"It's hard enough to make ends meet as it is. Mindnet bills, food, rent, all of that. It'd *help* if you worked on *your algorithm*, too. We need the extra income."

"But my algorithm isn't useful to these corporations." Eldat whined.

"You're going to have to adjust it into something the corporations *do* want. You can't just build algorithms for the greater good and expect to get paid."

"Ugh, why doesn't anyone want algorithms that study how people best learn things??"

"It doesn't make money. You have to make it figure out how to sell shit to people."

"But I don't wanna."

Fotee rolled their eyes and continued working.

Eldat stared up at the ceiling and daydreamed about seeing blue skies and getting away from the constant pinging of clients, junk, and acquaintances with "great ideas" for new apps.