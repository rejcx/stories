"I'd like to choose my own schedule."

"Well, you see - we have core hours, where we expect everybody to be present."

"What are the core hours?"

"10 am to 3 pm."

"Oh okay, so I could come in early and leave at 3 pm?"

"Well, no. If others saw you leaving at 3 pm, that would be bad..."

"But then core hours are over."

"Really, we'd want you to stay until 4."

"OK, can I work at the office from 10 am to 4 pm, then finish up my work in the evening?"

"Well, we want you to be in the office for eight hours every day."

"But... then what are core hours for?"

"Hours where there's a guarantee that everybody is present."

"If I can't leave before 4, then I have to be in by 8 am. I can't be in at 10 am, because there are meetings at 9 am."

"Yes, you could work 9 to 5, or 8 to 4, or 10 to 6. We like to be flexible."

"That's not much flexibility."

"If you have to take some time to go to a doctor appointment or pick up a kid from school, we're flexible on that, too. As long as you get your eight hours in that day."

"But I can't do that just for the sake of being productive."

"We want everybody in the office for core hours."

"And no leaving before 4."

"No leaving before 4."