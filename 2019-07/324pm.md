# 3:24 pm

I would really like to go home and re-energize

But instead I am writing as a disguise

I've worked 7 hours so far today

But the requirement is 8 hours with no leeway

I don't want to keep on, I'm between tasks

So instead I'm using a text editor as a mask

Maybe it looks and sounds as if I'm coding

Through the idleness and restlessness I feel myself eroding

Fill the time, take a break

All of this is for the sake

Of money and food and paying rent

There is no option to escape the system, to dissent

To not work is to starve

My own creative niche I cannot carve

As I worry about paying bills

And sit at work with time to kill
