# Stories

Just random little writings. I'm really insecure about trying to write stories,
and I stopped writing stories when I was a pre-teen. Still, I've been wanting
to be creative and I don't have enough time to do game development, or even to write comics,
so perhaps writing short stories would be a good way to get some creative momentum,
and just to satiate that need.

| Story     	    	 | Classification | Date    | Description				          | Link |
| ---------------------- | -------------- | ------- | --------------------------------------------------- | ---- |
| Freelancing Wizard 	 | Short story	  | 2019-06 | A broke wizard freelances odd jobs 	   	  |  [Freelancing Wizard](2019-06/freelancing-wizard.md) |
| The Past    		 | Short story	  | 2019-06 | A short glimpse at a history lesson from the future |  [The Past](2019-06/the-past.md) |
| Write What You Know	 | Short story	  | 2019-06 | A restless programmer 	   	       	   	  |  [Write What You Know](2019-06/write-what-you-know.md) |
